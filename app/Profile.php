<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $fillable = [
        'picture',
        'facebook_url',
        'twitter_url',
        'gitlab_url',
        'bio',
    ];
}
