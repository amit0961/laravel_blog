<?php

namespace App\Http\Controllers;

use App\Category;
use App\Post;
use App\Tag;
use App\Video;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class PublicController extends Controller
{
    public function index(){
        $data= [
            'posts'=> Post::paginate(3),
            'categories'=> Category::all()->toArray(),
            'tags'=> Tag::all()
        ];
        return view('frontend.pages.index', $data);
    }
    public function singlePage(Post $post){
        $data= [
            'categories'=> Category::all()->toArray(),
            'tags'=> Tag::all(),
            'post'=>$post,
        ];
        return view('frontend.pages.single', $data);
    }
    public function listPage($id){
        $posts = [];
        if(request()->segment('1') =='category'){
            $posts= Category::findOrFail($id)->posts()->paginate('5');
        }elseif((request()->segment('1') =='tag')){
            $posts= Tag::findOrFail($id)->posts()->paginate('5');
        }

        $data= [

            'posts'=> $posts,
            'categories'=> Category::all()->toArray(),
            'tags'=> Tag::all()
        ];
        return view('frontend.pages.index', $data);
    }
    public function videoPage(){
        $videos = [];
//        if(request()->segment('1') =='category'){
//            $posts= Category::findOrFail($id)->posts()->paginate('5');
//        }elseif((request()->segment('1') =='tag')){
//            $posts= Tag::findOrFail($id)->posts()->paginate('5');
//        }
        $videos= Video::paginate(2);
        $data= [

            'videos'=> $videos,
            'categories'=> Category::all()->toArray(),
            'tags'=> Tag::all()
        ];
        return view('frontend.pages.videoPage', $data);
    }
    public function videoDetails(Video $video){
        $data= [
            'categories'=> Category::all()->toArray(),
            'tags'=> Tag::all(),
            'video'=>$video,
        ];
        return view('frontend.pages.videoDetails', $data);
    }
}
