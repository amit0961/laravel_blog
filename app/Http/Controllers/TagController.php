<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Tag;

class TagController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){

        $paginatePerpage =5;
        $pageNumber = request('page');
        if (!is_null($pageNumber)){
            $serial= $paginatePerpage * $pageNumber - $paginatePerpage;
        }
        else{
            $serial=0;
        }


        $keyword = request('keyword');
        if(!is_null($keyword)){
            $tags =Tag::where('title', 'Like', "%{$keyword}%")->paginate($paginatePerpage);
        }
        else{
            $tags = Tag::paginate($paginatePerpage);
        }


        return view('backend.tags.index', compact('tags','serial'));
    }
    public function create(){
        return view('backend.tags.create');
    }
    public function store(Request $request){
        $data= $request->all();
        $data['created_by']= auth()->user()->id;
        Tag::create($data);
        session()->flash('message', 'Created Successfully!');
        return redirect()->route('tags.index');
    }
    public function show($id){
        $tag = Tag::findOrFail($id);
        return view('backend.tags.show', compact('tag'));
    }
    public function edit($id){
        $tag = Tag::findOrFail($id);
        return view('backend.tags.edit', compact('tag'));

    }
    public function update(Request $request, Tag $tag){
//        $tag = Tag::findOrFail($id);
        $data=$request->all();
        $data['updated_by']=auth()->user()->id;
        $tag->update($data);
        session()->flash('message', 'Updated Successfully!');
        return redirect()->route('tags.index');

    }
    public function destroy($id){
        Tag::destroy($id);
        session()->flash('message', 'Removed Successfully!');
        return redirect()->route('tags.index');
    }
    public function trash(){
        $paginatePerpage =5;
        $pageNumber = request('page');
        if (!is_null($pageNumber)){
            $serial= $paginatePerpage * $pageNumber - $paginatePerpage;
        }
        else{
            $serial=0;
        }

        $tags = Tag::onlyTrashed()->paginate($paginatePerpage);


        return view('backend.tags.trash', compact('tags','serial'));


    }
    public function restore($id){
        $tag = Tag::onlyTrashed()
            ->where('id', $id)
            ->first();
        $tag->restore();
        session()->flash('message', 'Restore Successfully!');
        return redirect()->back();

    }
    public function delete($id){
        $tag = Tag::onlyTrashed()
            ->where('id', $id)
            ->first();
        $tag->forceDelete();
        session()->flash('message', 'Deleted Successfully!');
        return redirect()->back();

    }
}
