<?php

namespace App\Http\Controllers;

use App\Category;
use App\Tag;
use App\Video;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class VideoController extends Controller
{
    public function index(){
        $paginatePerpage =5;
        $pageNumber = request('page');
        if (!is_null($pageNumber)){
            $serial= $paginatePerpage * $pageNumber - $paginatePerpage;
        }
        else{
            $serial=0;
        }


        $keyword = request('keyword');
        if(!is_null($keyword)){
            $videos =Video::latest()->where('title', 'Like', "%{$keyword}%")->paginate($paginatePerpage);
        }
        else{
            $videos = Video::latest()->paginate($paginatePerpage);
        }


        return view('backend.videos.index', compact('videos','serial'));
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::pluck('title', 'id');
        $tags= Tag::get(['title', 'id']);
        return view('backend.videos.create', compact('categories','tags'));
    }

    public function store(Request $request)
    {
        $data = $request->all('');
        $data['created_by'] = auth()->user()->id;

        $video = Video::create($data);
        $video->tags()->attach($request->tag_ids);

        Session::flash('message', 'Created Successfully');
        return redirect()->route('videos.index');
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {


        return view('backend.posts.show', compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        $categories = Category::pluck('title', 'id');
        $tags = Tag::get(['title', 'id']);
        $selectedTags = $post->tags()->pluck('title', 'id')->toArray();
        return view('backend.posts.edit', compact('post', 'categories','tags','selectedTags'));
    }

    public function update(Request $request, Post $post)
    {
//        $post = Video::findOrFail($id);

        $data = $request->except('image');
        $data['updated_by'] = auth()->user()->id;
        if($request->hasFile('image')){
            $data['image'] = $this->uploadImage($request->image);
        }

        $post->update($data);

        $post->tags()->sync($request->tag_ids);

        Session::flash('message', 'Updated Successfully');

        return redirect()->route('posts.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function destroy(Post $post)
    {
        $post->delete();

        Session::flash('message', 'Deleted Successfully');
        return redirect()->route('posts.index');
    }
//
//    /**
//     * @param $file
//     * @return string
//     */
//    private function uploadImage($file)
//    {
//        $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());//formatting the name for unique and readable
//        $file_name =  $timestamp.'.'.$file->getClientOriginalExtension();
//        Image::make($file)->resize(750, 300)->save(public_path() . self::UPLOAD_DIR . $file_name);
//        return $file_name;
//    }
}
