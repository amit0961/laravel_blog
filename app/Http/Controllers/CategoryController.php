<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function index(){

        $paginatePerpage =5;
        $pageNumber = request('page');
        if (!is_null($pageNumber)){
            $serial= $paginatePerpage * $pageNumber - $paginatePerpage;
        }
        else{
            $serial=0;
    }


        $keyword = request('keyword');
        if(!is_null($keyword)){
            $categories =Category::where('title', 'Like', "%{$keyword}%")->paginate($paginatePerpage);
        }
        else{
             $categories = Category::paginate($paginatePerpage);
        }


        return view('backend.categories.index', compact('categories','serial'));
    }
    public function create(){
        return view('backend.categories.create');
    }
    public function store(Request $request){
        Category::create($request->all());
        session()->flash('message', 'Created Successfully!');
        return redirect()->route('categories.index');
    }
    public function show($id){
        $category = Category::findOrFail($id);
        return view('backend.categories.show', compact('category'));
    }
    public function edit($id){
        $category = Category::findOrFail($id);
        return view('backend.categories.edit', compact('category'));

    }
    public function update(Request $request, $id){
        $category = Category::findOrFail($id);
        $category->update($request->all());
        session()->flash('message', 'Updated Successfully!');
        return redirect()->route('categories.index');

    }
    public function destroy($id){
        Category::destroy($id);
        session()->flash('message', 'Removed Successfully!');
        return redirect()->route('categories.index');
    }
    public function trash(){
        $paginatePerpage =5;
        $pageNumber = request('page');
        if (!is_null($pageNumber)){
            $serial= $paginatePerpage * $pageNumber - $paginatePerpage;
        }
        else{
            $serial=0;
        }

        $categories = Category::onlyTrashed()->paginate($paginatePerpage);


        return view('backend.categories.trash', compact('categories','serial'));


    }
    public function restore($id){
        $category = Category::onlyTrashed()
            ->where('id', $id)
            ->first();
        $category->restore();
        session()->flash('message', 'Restore Successfully!');
        return redirect()->back();

    }
    public function delete($id){
        $category = Category::onlyTrashed()
            ->where('id', $id)
            ->first();
        $category->forceDelete();
        session()->flash('message', 'Deleted Successfully!');
        return redirect()->back();

    }




}
