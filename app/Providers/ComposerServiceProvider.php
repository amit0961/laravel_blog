<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;

class ComposerServiceProvider extends ServiceProvider
{

    public function boot()
    {
        View::composer(
            'backend.layouts.master', 'App\Http\ViewComposers\TemplateComposer'
        );
    }
    public function register()
    {
        //
    }
}
