<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//ekhane nicher router namer oikhane >>> admin <<<hbe now for this direct page link;
Route::get('/', 'PublicController@index')->name('firstPage');

Route::get('/single/{post}', 'PublicController@singlePage')->name('singlePage');
Route::get('/category/{category}', 'PublicController@listPage')->name('listPage');
Route::get('/tag/{tag}', 'PublicController@listPage')->name('listPageTag');
Route::post('/content/{id}', 'CommentController')->name('comment');
Route::get('/video', 'PublicController@videoPage')->name('videoPage');
Route::get('/video/{video}', 'PublicController@videoDetails')->name('videoDetails');


Route::get('/admin', function () {
    return view('backend.home');
});


Route::get('/categories', 'CategoryController@index')->name('categories.index');
Route::get('/categories/trash', 'CategoryController@trash')->name('categories.trash');
Route::get('/categories/trash/{id}/restore', 'CategoryController@restore')->name('categories.restore');
Route::delete('/categories/trash/{id}/delete', 'CategoryController@delete')->name('categories.delete');

Route::get('categories/create', 'CategoryController@create')->name('categories.create');
Route::post('/categories', 'CategoryController@store')->name('categories.store');
Route::get('categories/{data}/edit', 'CategoryController@edit')->name('categories.edit');
Route::put('categories/{data}', 'CategoryController@update')->name('categories.update');

Route::get('categories/show/{data}', 'CategoryController@show')->name('categories.show');
Route::delete('categories/{data}', 'CategoryController@destroy')->name('categories.destroy');
//Route::get('categories/single', 'CategoriesController@show')->name('categories.single');
//Route::get('/admin', function () {
//    return view('backend.categories.index');
//});
//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>for post<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
Route::resource('posts','PostController');
Route::resource('tags','TagController');
Route::resource('videos','VideoController');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
