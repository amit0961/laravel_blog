@extends('backend.layouts.master')
@section('content')

    <div class="card">
        <div class="card-header d-inline" >
            <h1 class="d-inline">Posts List</h1>
            <div class="float-right">
{{--                <a href="{{route('posts.trash')}}"><button  type="button" class="btn btn-primary">Trash</button></a>--}}
                <a href="{{route('posts.create')}}"><button  type="button" class="btn btn-primary">Add New</button></a>
            </div>
        </div>

        @if (session()->has('message'))
            {{session('message')}}
        @endif
        <div class="card-body">
{{--            <div >--}}
{{--            <!-- Search-from -->--}}
{{--            {!! Form::open([ 'route' => 'posts.index' ,--}}
{{--                'class'=>'form-inline',--}}
{{--                'method'=> 'get',--}}

{{--            ]) !!}--}}
{{--            <form action="#" method="post" class="form-inline ">--}}

{{--            {!! Form::text('keyword',null,[--}}
{{--            'class'=> 'form-control',--}}
{{--            'placeholder'=>'Search',--}}
{{--                ]) !!}--}}
{{--            <input class="form-control " v-model="search" id="search" type="search" placeholder="Search" name="keyword" required="">--}}
{{--                <button class="btn btn-style " type="submit">Search</button>--}}
{{--            {!! Form::button('Search',[--}}
{{--            'class'=>'btn btn-danger',--}}
{{--            'type'=>'submit'--}}
{{--            ]) !!}--}}

{{--            {!! Form::close() !!}--}}

{{--            <!--// Search-from -->--}}
{{--            </div>--}}
            <table class="table table-bordered table-hover" >
                <thead class="table-primary text-center">
                <tr>
                        <th style="width: 10%">#SL</th>
                        <th style="width: 60%">Title</th>
                        <th style="width: 30%">Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach( $posts as $post)
                <tr class="text-center">
                    <td>{{++ $serial}}</td>
                    <td> {{ $post->title}}</td>
                    <td>
                        <a class="btn btn-primary btn-sm" href="{{ route('posts.show', $post->id)}}">Show</a>||
                        <a class="btn btn-success btn-sm" href="{{ route('posts.edit', $post->id)}}">Edit</a>||
                        {!! Form::open([
                            'route'=>['posts.destroy', $post->id],
                            'method'=> 'delete',
                            'style'=> 'display:inline'
                        ]) !!}
                        {!! Form::button('Remove', [
                            'type'=> 'submit',
                            'class'=>'btn btn-danger btn-sm',
                            'onclick'=> 'return confirm("Are You Sure , Want To Removed It?" )',
                        ]) !!}
                        {!! Form::close() !!}
                    </td>
                </tr>
                    @endforeach
                </tbody>
                </table>

             </div>
        <div >
            {{ $posts->links() }}
        </div>
    </div>

    @stop
