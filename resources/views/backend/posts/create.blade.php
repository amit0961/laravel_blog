@extends('backend.layouts.master')
@section('content')
    <div class="card">
        <div class="card-header d-inline">
            <h1 class="d-inline">Posts ADD</h1>
            <div class="float-right">
                <a href="{{route('posts.index')}}">
                    <button type="button" class="btn btn-primary">List</button>
                </a>
            </div>
        </div>
        <div class="card-body">
            <div class="form-group">
                {!! Form::open([
                'route' => 'posts.store',
                'files'=>'true'
                 ]) !!}
                <div class="form-group row">
                    {!! Form::label('Title', null, ['class' => 'col-sm-2 col-form-label']) !!}
                    <div class="col-sm-10">
                        {!! Form::text('title', null, [
                                'class' => 'form-control',
                                'id' => 'Title',
                            ]) !!}<br>
                        @if ($errors->has('title'))
                            {{ $errors->first('title') }}
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    {{--<label for="title" class="col-sm-2 col-form-label">Title</label>--}}
                    {!! Form::label('category_id', null, ['class' => 'col-sm-2 col-form-label']) !!}

                    <div class="col-sm-10">
                        {!! Form::select('category_id', $categories, null, [
                                'placeholder' => 'Select Category',
                                'class' => 'form-control',
                                'id' => 'category_id',
                            ]) !!}<br>
                        @if ($errors->has('category_id'))
                            {{ $errors->first('category_id') }}
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    {{--<label for="title" class="col-sm-2 col-form-label">Title</label>--}}
                    {!! Form::label('description', null, ['class' => 'col-sm-2 col-form-label']) !!}

                    <div class="col-sm-10">
                        {!! Form::textarea('description', null, [
                                'class' => 'form-control',
                                'id' => 'description',
                            ]) !!}<br>
                        @if ($errors->has('description'))
                            {{ $errors->first('description') }}
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    {!! Form::label('image', null, ['class' => 'col-sm-2 col-form-label']) !!}
                    <div class="col-sm-10">
                        {!! Form::file('image', [
                                'class' => 'form-control',
                                'id' => 'image',

                            ]) !!}
                        <br>
                        @if ($errors->has('image'))
                            {{ $errors->first('image') }}
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    {!! Form::label('tag_id', 'Tags', ["class" => "col-md-2 col-form-label text-md-right"]) !!}
                    <div class="col-md-10">
                        @foreach ($tags as $tag)
                            <div class="checkbox">
                                <label>
                                    {{ Form::checkbox('tag_ids[]', $tag->id, false, ['class' => 'field']) }}
                                    {{ $tag->title }}
                                </label>
                            </div>
                        @endforeach
                    </div>
                </div>
                {{--in_array($tag->id, $selectedTagIds--}}
                {{--ekanebakitukusesh--}}
                {!! Form::button('Add', [
                                            'class' => 'btn btn-primary',
                                            'type' => 'submit',
                                        ]) !!}
                {!! Form::close()  !!}

            </div>
        </div>
    </div>
@stop
