@extends('backend.layouts.master')
@section('content')
    <div class="card">
        <div class="card-header d-inline" >
            <h1 class="d-inline">Categories ADD</h1>
            <div class="float-right">
                <a href="{{route('categories.index')}}"><button  type="button" class="btn btn-primary">List</button></a>

            </div>
        </div>
        <div class="card-body">
            <div class="form-group">
                {!!   Form::open(['route'=>'categories.store'])   !!}

                    <h3> Titles are here: </h3>

                    {!! Form::text( 'title', null, [
                        'class'=>'form-control',
                        'style' => 'width: 75%',
                        'placeholder' => 'Enter the Categories here',
                        'required'
                     ]) !!}

                {!! Form::submit('ADD' ,[
                'class' => 'btn btn-success'
                ]) !!}

                {!! Form::close()  !!}

            </div>



        </div>
    </div>

@stop
