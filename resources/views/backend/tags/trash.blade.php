@extends('backend.layouts.master')
@section('content')
    <div class="card">
        <div class="card-header d-inline" >
            <h1 class="d-inline">Deleted Tags </h1>
            <div class="float-right">
                <a href="{{route('tags.index')}}"><button  type="button" class="btn btn-primary">List</button></a>
            </div>
        </div>

        @if (session()->has('message'))
            {{session('message')}}
        @endif


        <div class="card-body">
            <table class="table  table-bordered table-hover" >
                <thead class="table-primary text-center">
                <tr>
                    <th style="width: 10%">#SL</th>
                    <th style="width: 60%">Title</th>
                    <th style="width: 30%">Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach( $tags as $tag)
                    <tr class="text-center">
                        <td>{{++ $serial}}</td>
                        <td> {{ $tag->title}}</td>
                        <td>
                            <a class="btn btn-primary btn-sm" href="{{ route('tags.show', $tag->id)}}">Show</a>||
                            <a class="btn btn-success btn-sm" href="{{ route('tags.restore', $tag->id)}}">Restore</a>||
                            {!! Form::open([
                                'route'=>['tags.delete', $tag->id],
                                'method'=> 'delete',
                                'style'=> 'display:inline'
                            ]) !!}
                            {!! Form::button('Delete', [
                                'type'=> 'submit',
                                'class'=>'btn btn-danger btn-sm',
                                'onclick'=> 'return confirm("Are You Sure , Want To Delete It Permanently?" )',
                            ]) !!}

                            {!! Form::close() !!}
                        </td>
                    </tr>
                @endforeach

                </tbody>

            </table>
        </div>
        <div class="float-right">
            {{ $tags->links() }}
        </div>
    </div>

@stop
