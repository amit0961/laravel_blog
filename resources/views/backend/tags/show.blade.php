@extends('backend.layouts.master')
@section('content')
    <div class="card">
        <div class="card-header d-inline" >
            <h1 class="d-inline">Tags ADD</h1>
            <div class="float-right">
                <a href="{{route('tags.index')}}"><button  type="button" class="btn btn-primary">List</button></a>

            </div>
        </div>
        <div class="card-body">
            <div class="form-group">
                <table class="table  table-bordered table-hover" >

                    <thead class="table-primary text-center">

                <tr >
                    <td scope="col">Title</td>
                    <td scope="col">{{ $tag->title}}</td>
                </tr>
                <tr>
                    <td scope="col">Created At </td>
                    <td scope="col">{{ $tag->created_at}}</td>

                </tr>
                    </thead>
                </table>


            </div>
        </div>
    </div>

@stop
