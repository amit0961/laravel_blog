@extends('backend.layouts.master')
@section('content')
    <div class="card">
        <div class="card-header d-inline" >
            <h1 class="d-inline">Tags ADD</h1>
            <div class="float-right">
                <a href="{{route('tags.index')}}"><button  type="button" class="btn btn-primary">List</button></a>

            </div>
        </div>
        <div class="card-body">
            <div class="form-group">
                {!!   Form::open([
                      'route' => ['tags.update', $tag->id],
                      'method'=> 'put',
                  ]) !!}
                 <h3> EDIT PAGE:- </h3>

                  {!! Form::text( 'title',$tag->title,  [
                        'class'=>'form-control',
                        'style' => 'width: 75%',
                        'placeholder' => 'Enter the Tags here',
                        'required'
                 ]) !!}

                {!! Form::submit('Update' ,[
                'class' => 'btn btn-success'
                ]) !!}

                {!! Form::close()  !!}

            </div>



        </div>
    </div>

@stop
