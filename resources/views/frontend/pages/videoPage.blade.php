@extends('frontend.layouts.master')
@section('title', 'Home')
@section('content')
    <h1 class="my-4">Page Heading
        <small>Secondary Text</small>
    </h1>

    <!-- Blog Post -->
    @foreach($videos as $video)
        <div class="card mb-4">
            {!! $video->embed_code !!}
           <div class="card-body">
                <h2 class="card-title">{{$video->title}}</h2>
{{--                <p class="card-text">{!! str_limit($video->summary, 250) !!}</p>--}}
                <a href="{{route('videoDetails', $video->id)}}" class="btn btn-primary">Read More &rarr;</a>
            </div>
            <div class="card-footer text-muted">
                Posted on {{$video->created_at->toFormattedDateString()}}
{{--                <a href="#">{{$video->creator->name}}</a>--}}
            </div>
        </div>
    @endforeach
    <!-- Pagination -->
    <ul class="pagination justify-content-center mb-4">
        <li class="page-item">
            {{$videos->links()}}
        </li>
    </ul>
@stop
