@extends('frontend.layouts.master')
@section('title', 'Video-Details')
@section('content')
    <!-- Title -->
    <h1 class="mt-4">{{ $video->title }}</h1>

    <hr>

    <!-- Date/Time -->
    <p>Posted on {{ $video->created_at->toFormattedDateString() }} at {{ $video->created_at->toTimeString()}}</p>

    <hr>

    <!-- Preview Image -->
    {!! $video->embed_code !!}

    <hr>

    <!-- Post Content -->
    {!! $video->description !!}
    <hr>
    <p><strong>Tags:-</strong>
        @foreach($video->tags as $tag)
            <a href="#">{!! $tag->title !!}</a>>>
            @endforeach
    </p>
    <hr>
    {{--    <blockquote class="blockquote">--}}
    {{--        <p class="mb-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>--}}
    {{--        <footer class="blockquote-footer">Someone famous in--}}
    {{--            <cite title="Source Title">Source Title</cite>--}}
    {{--        </footer>--}}
    {{--    </blockquote>--}}
    <!-- Comments Form -->
    <h3>Leave your comments:</h3>
    @if(auth()->check())
        <div class="card my-4">
            <h5 class="card-header">Leave a Comment:</h5>
            <div class="card-body">
                {!! Form::open([ 'route'=>['comment', $video->id] ]) !!}
                {!! Form::hidden('commentable_type', 'Video') !!}
                <div class="form-group">
                    <textarea name="body" class="form-control" rows="3"></textarea>
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
                {!! Form::close() !!}
            </div>
        </div>
    @else
        <a href="{{route('login')}}"><button class="btn btn-sm btn-outline-success">Login</button> </a> to leave a comment.
        <hr>
    @endif

    @foreach($video->comments as $comment)
        <!-- Single Comment -->
        <div class="media mb-4">
            <img class="d-flex mr-3 rounded-circle" height="50" width="50" src="{{asset('uploads/users/'.$comment->commentedBy->profile->picture)}}" alt="">
            <div class="media-body">
                <h5 class="mt-0"> {{ $comment->commentedBy->name }}</h5>
                {{ $comment->body }}
            </div>
        </div>
    @endforeach
    <!-- Comment with nested comments -->
    {{--    <div class="media mb-4">--}}
    {{--        <img class="d-flex mr-3 rounded-circle" src="http://placehold.it/50x50" alt="">--}}
    {{--        <div class="media-body">--}}
    {{--            <h5 class="mt-0">Commenter Name</h5>--}}
    {{--            Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.--}}

    {{--            <div class="media mt-4">--}}
    {{--                <img class="d-flex mr-3 rounded-circle" src="http://placehold.it/50x50" alt="">--}}
    {{--                <div class="media-body">--}}
    {{--                    <h5 class="mt-0">Commenter Name</h5>--}}
    {{--                    Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.--}}
    {{--                </div>--}}
    {{--            </div>--}}

    {{--            <div class="media mt-4">--}}
    {{--                <img class="d-flex mr-3 rounded-circle" src="http://placehold.it/50x50" alt="">--}}
    {{--                <div class="media-body">--}}
    {{--                    <h5 class="mt-0">Commenter Name</h5>--}}
    {{--                    Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.--}}
    {{--                </div>--}}
    {{--            </div>--}}

    {{--        </div>--}}
    {{--    </div>--}}
@stop
